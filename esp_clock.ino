#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include <RTClib.h>

RTC_DS3231 rtc;  // Instancia para el módulo de reloj en tiempo real

const char *ssid = "Name WiFi";
const char *password = "Password";

const char *apiURL = "http://esp32webcontrol.000webhostapp.com/api.php";

// Crea una instancia de WiFiClient
WiFiClient wifiClient;
HTTPClient http;

// Vectores globales para almacenar los datos
std::vector<int> horas;
std::vector<int> minutos;

const int pinRelay = D3;  // Pin al que está conectado el relé

int activaciones = sizeof(horas) / sizeof(horas[0]);  // Número de activaciones programadas

void setup() {
  Serial.begin(115200);
  pinMode(pinRelay, OUTPUT);
  digitalWrite(pinRelay, LOW);

  // Conéctate a la red WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Conectando a WiFi...");
  }

  if (!rtc.begin()) {
    Serial.println("¡No se pudo encontrar el RTC!");
    while (1);
  }

  // Asegúrate de que el RTC tenga la hora correcta cargada
  if (rtc.lostPower()) {
    Serial.println("RTC perdió la alimentación, ¡ajusta la hora!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }

  Serial.println("Conexión exitosa");
  digitalWrite(pinRelay, LOW);

  readDataFromServer();
}

void loop() {
  DateTime now = rtc.now();
  Serial.print("\nHoras: ");
  Serial.print(now.hour());
  Serial.print(" Minutos: ");
  Serial.println(now.minute());

  // Comprueba todas las activaciones programadas
  for (int i = 0; i < activaciones; i++) {
    
    if (now.hour() == horas[i] && now.minute() == minutos[i]) {
      Serial.println("Hora correcta");
      if (now.second() <= 5) {
        digitalWrite(pinRelay, HIGH);
        Serial.println("Rele activo");
        break;  // Sale del bucle si encuentra una coincidencia
      }
    } else {
      Serial.println("False");
      digitalWrite(pinRelay, LOW);
      Serial.println("Rele Inactivo");
    }
    delay(200);
  }
  delay(800);  // Retardo de 1 segundo entre iteraciones
}

void readDataFromServer() {
  // Realiza la solicitud GET al servidor
  http.begin(wifiClient, apiURL);

  int httpResponseCode = http.GET();

  if (httpResponseCode > 0) {
    // Parsea el JSON recibido
    DynamicJsonDocument doc(1024);  // Ajusta el tamaño según tus necesidades
    deserializeJson(doc, http.getString());

    // Accede a los datos y guárdalos en vectores
    JsonArray array = doc.as<JsonArray>();
    for (JsonVariant value : array) {
      int hora = value["hora"].as<int>();
      int minuto = value["minuto"].as<int>();

      // Almacena los datos en los vectores globales
      horas.push_back(hora);
      minutos.push_back(minuto);
      activaciones++;
    }
  } else {
    Serial.print("Error en la solicitud. Código de respuesta: ");
    Serial.println(httpResponseCode);
  }

  http.end();
}
