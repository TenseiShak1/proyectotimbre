# **Manual de Usuario del Timbre Escolar con NodeMCU**

## Introducción 

Bienvenido al Manual de Usuario del Timbre Escolar con NodeMCU. Este documento tiene como objetivo proporcionar una guía completa para la instalación, configuración y uso eficiente de nuestro innovador sistema de timbre escolar basado en la tecnología NodeMCU.

**Proposito general**

El Timbre Escolar con NodeMCU es una solución moderna y eficaz para la gestión del horario de timbre en entornos escolares. Este sistema automatizado no solo simplifica la operación del timbre, sino que también brinda flexibilidad y control precisos sobre los horarios, adaptándose a las necesidades específicas de cada institución educativa.

Este manual está diseñado para orientar a administradores, personal docente y técnicos a través de la implementación y operación exitosa del Timbre Escolar con NodeMCU. A lo largo de las secciones, encontrará instrucciones detalladas, consejos prácticos y soluciones para posibles inconvenientes.

Esperamos que esta solución no solo mejore la eficiencia en la gestión del timbre escolar, sino que también contribuya a un ambiente educativo más organizado y armonioso. Antes de comenzar, asegúrese de contar con todos los requisitos previos y siga cada paso detenidamente para aprovechar al máximo todas las capacidades de nuestro sistema de timbre escolar. ¡Gracias por elegirnos para optimizar la experiencia en su institución educativa!

# Requisitos 

### Componentes Utilizados

* Nodemcu.(1)
* Modulo RTC DS3231.(1)
* Relay de 5 voltios.(1)
* Resistencias de 1000 ohmnios.(2)
* Diodo led.(1)
* Diodo 1n4148.(1)
* Diodo 1n4007.(1)
* Transistor BC547.(1)
* Regleta de pines Hembra.(1) 
* Boton tipo Switch.(1)
* Fuente de alimentacion de 5 voltios con 1 amperio.(1)
* Entrada Hembra de la fuente de alimentacioo.(1)
* Baquela perforada de 9cmX15cm.(1)
* Caja de 11cmX16cm

### Software utilizado

* Arduino IDE.
* Python v3.x . 
* Php
* Google Chrome

### Implementación de componentes

**Esquema de conexión**
![image](/image/Esquema.png)

**Diseño en fisico**
![image](/image/Implementacion.jpeg)

### Implementación de software

**Arduino IDE**

![image](/image/Esp_clock_diagram.png)

Nota: Si se actualiza el registro en la web se debe reiniciar la NodeMCU mediante el Switch que se implemento en la caja.

**Host de la Web**

Para el proyecto se implemento el servicio de alojamiento web que ofrece 000WebHost que nos permite replicar nuestra web diseñada en local, poder pasarla a internet y poder realizar registros en cualquier navegador sin estar atados a un equipo de  computo especifico.

La web tiene un diseño bastante sensillo con el cual se logra hacer registros para el timbre y se compone de 2 apartados los cuales son:

- **Formulario de datos**
- **Tabla de registros**

En el Formulario se implemento 2 entradas de datos y un botón el cual hace la petición de guardado a la base de datos como se muestra a continuación:

![image](/image/IngresarDato.png)


Mientras que en la tabla de registro se aplico una tabla de 4 columnas.

![image](/image/Registro.png)

Primera columna es "id" la cual no da un identificador unico a dato que se ingresa con lo cual luego si ya no se requiero se puede elminiar mediante ese valor unico.

Segunda columna es "Hora", el ingreso de la hora en el formato 24Hrs.

Tercera columna es "Minutos", el formato de numero es 0 a 59 minutos.

Cuarta columna es un enlace que activa una petición a base de datos para eliminar el registro especifico.

Recomendación: El modulo RTC tiene un retraso interno de 1 minuto con 20 segundos, por si tiene retraso al momento de usarse 
